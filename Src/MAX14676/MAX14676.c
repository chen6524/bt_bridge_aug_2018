/***************************************************************************
 *
 *            Copyright (c) 2012 by Artafelx INC.
 *
 * This software is copyrighted by and is the sole property of
 * Artaflex INC.  All rights, title, ownership, or other interests
 * in the software remain the property of Artaflex INC.  This
 * software may only be used in accordance with the corresponding
 * license agreement.  Any unauthorized use, duplication, transmission,
 * distribution, or disclosure of this software is expressly forbidden.
 *
 * This Copyright notice may not be removed or modified without prior
 * written consent of Artaflex INC.
 *
 * Artaflex INC reserves the right to modify this software without notice.
 *
 * Artaflex INC.
 * 96 Steelcase Road West,
 * Markham, ON L3R 3T9
 * Canada
 *
 * Tel:   (905) 470-0109
 * http://www.artaflex.com

  
 *
 ***************************************************************************/

#include "MAX14676.h"
#include "i2c.h"
#include "iwdg.h"
#include "lptim.h"
//#include "gpio.h"

uint8_t  max14676Exist       = 0;
uint16_t max14676Voltage     = 0x1FFF;
uint16_t max14676Voltage_old = 0x1FFF;                        // 2014.12.08
uint8_t  max14676Charge      = 0;                             //init to 50%
uint8_t  max14676ChargeStat  = 0;
uint8_t  max14676_result;

HAL_StatusTypeDef hal_status;
void max14676_WriteReg(uint8_t regAddr,uint8_t regValue) 
{
  uint8_t send_Buf[3];
    
  send_Buf[0] = regAddr;
  send_Buf[1] = (uint8_t)(regValue&0xff);
	
	//(void)HAL_I2C_Mem_Write(&hi2c1, MAX14676_DEV_ADDR, regValue, MemAddSize, &send_Buf[1], 1, 0xFFFF)
	HAL_I2C_Master_Transmit(&hi2c1, (uint16_t)MAX14676_DEV_ADDR, send_Buf, 2, 0xFFFF);
	
//  (void)I2C_driver_SelectSlave(MAX14676_DEV_ADDR);  
//  (void)I2C_driver_SendBlock(send_Buf,2,&max14676_result);    
//  (void)I2C_driver_SendStop();
}

uint8_t max14676_ReadReg(uint8_t regAddr, uint8_t* ptrVal) 
{
  uint8_t result = 0;
  uint8_t readValue[2];
   	
//result  = I2C_driver_SelectSlave(MAX14676_DEV_ADDR);
//result |= I2C_driver_SendBlock(&regAddr,1,&max14676_result);  
//result |= I2C_driver_RecvBlock(readValue,1,&max14676_result);
//(void)I2C_driver_SendStop();     
    
	hal_status = HAL_I2C_Master_Transmit(&hi2c1, (uint16_t)MAX14676_DEV_ADDR, &regAddr, 1, 0xFFFF);
	hal_status = HAL_I2C_Master_Receive(&hi2c1, (uint16_t)MAX14676_DEV_ADDR, readValue, 1, 0xFFFF);	
	
  *ptrVal = readValue[0];
  return  result; 
}

uint8_t max14676_Read(uint8_t regAddr) 
{
//uint8_t result = 0;
  uint8_t readValue[2];
     
	hal_status = HAL_I2C_Master_Transmit(&hi2c1, (uint16_t)MAX14676_DEV_ADDR, &regAddr, 1, 0xFFFF);
	hal_status = HAL_I2C_Master_Receive(&hi2c1, (uint16_t)MAX14676_DEV_ADDR, readValue, 1, 0xFFFF);	
	
  return  readValue[0]; 
}


void max14676_WriteReg2(uint8_t regAddr,uint16_t regValue) 
{
  uint8_t send_Buf[3];
    
  send_Buf[0] = regAddr;
  send_Buf[1] = (uint8_t)(regValue&0xff);
  send_Buf[2] = (uint8_t)(regValue>>8);
    
//  (void)I2C_driver_SelectSlave(MAX14676_DEV_ADDR2);  
//  (void)I2C_driver_SendBlock(send_Buf,3,&max14676_result);    
//  (void)I2C_driver_SendStop();
	
	HAL_I2C_Master_Transmit(&hi2c1, (uint16_t)MAX14676_DEV_ADDR2, send_Buf, 3, 0xFFFF);
}

uint8_t max14676_ReadReg2(uint16_t * pReadVal, uint8_t regAddr) 
{
  volatile uint8_t result = 0;
  uint8_t readValue[4];
   
//  result  = I2C_driver_SelectSlave(MAX14676_DEV_ADDR2);
//  result |= I2C_driver_SendBlock(&regAddr,1,&max14676_result);  
//  result |= I2C_driver_RecvBlock(readValue,4,&max14676_result);
//  (void)I2C_driver_SendStop();         

	hal_status = HAL_I2C_Master_Transmit(&hi2c1, (uint16_t)MAX14676_DEV_ADDR, &regAddr, 1, 0xFFFF);
  hal_status = HAL_I2C_Master_Receive(&hi2c1, (uint16_t)MAX14676_DEV_ADDR, readValue, 4, 0xFFFF);		
  
  *pReadVal = (uint16_t)readValue[2]*256 +(uint16_t)readValue[1]; 
  return result;
}

void ReStart_Process(void)
{
		if(HAL_GPIO_ReadPin(BTN_GPIO_Port, GPIO_PIN_3) == GPIO_PIN_RESET)
		{
			uint8_t ii = 00;
			HAL_Delay(100);
			if(HAL_GPIO_ReadPin(BTN_GPIO_Port, GPIO_PIN_3) == GPIO_PIN_RESET)
			{
				while(++ii < 20)
				{
					MX_IWDG_RESET();
					HAL_Delay(100);
					if(HAL_GPIO_ReadPin(BTN_GPIO_Port, GPIO_PIN_3) == GPIO_PIN_RESET)
					{
						
					}
					else
					{
						max14676_poweroff();
					}
				}
				BUZZ_On();
			}
			else
			{
				max14676_poweroff();
			}
		}
		else
		{
			max14676_poweroff();
		}	 
}

uint8_t status;
uint32_t RCC_CSR_reg = 0;
void max14676_init(void) 
{
   max14676_GetVer();
   if(!max14676Exist) return;

   status = max14676_Read(MAX14676_INT_A);                       // Clear All interrupt flags
	 status = max14676_Read(MAX14676_INT_B);
 //status = max14676_Read(MAX14676_STATUS_A);
 //status = max14676_Read(MAX14676_STATUS_B);

   max14676_WriteReg(MAX14676_PWR_CFG_ADDR,  0x80); 
   max14676_WriteReg(MAX14676_CHGCNTLB,      0x10);              // 0x10 --> 7.5mA,  0x11 -- > 15mA
   max14676_WriteReg(MAX14676_ILIMCNTL_ADDR, 0x17);              // 00010111, 101->1000mA, 11 -> 
   max14676_WriteReg(MAX14676_CHGCNTLA,      0x1C);//0x18        // 0x0B  = 0x00010100 (0x14)   --->250mA
                                                                 //       = 0x00011100 (0x1C)   --->350mA	
   //max14676_WriteReg(MAX14676_LDOCFG,0x40);//0x18
   //data = max14676_ReadReg(0x0);
   //data = max14676_ReadReg(0x05);
   //max14676_WriteReg(MAX14676_PWR_CFG_ADDR, 0x80); 
   //max14676_WriteReg(0x11, 0x84); 
   max14676_WriteReg(MAX14676_PCHGCNTL,      0x6e);              // 0 11 01 110, 11->40mA, 110 -> 2.90V     
	 
	 max14676_WriteReg(MAX14676_CDETCNTLB,     0x84);              // Charger automatically restarts when Vbat drops below     	 
	 max14676_WriteReg(MAX14676_CHGVSET,       0xC0);              // 220mV Recharge Threshold in Relation to VBAT
	 
  //testing
   //data =max14676_ReadReg2(0x02);  
   //data =max14676_ReadReg2(0x04);		

   status = max14676_Read(MAX14676_INT_MASKA);	 
	 max14676_WriteReg(MAX14676_INT_MASKA,     status | CHGSTAT_MASK);     // Enable ChgStat Interrupt
	 
   status = max14676_Read(MAX14676_INT_MASKB);	 
	 max14676_WriteReg(MAX14676_INT_MASKB,     USBOK_MASK);        // Enable USBOK Interrupt
	 
	 status = max14676_Read(MAX14676_STATUS_A);
	 if(status == CHARGER_OFF)
	 {
		 ChagerStatusFlag = 0;
	 }
	 else if(status == CHARGER_TIMER_DONE)
	 {
		 ChagerStatusFlag = 1;
	 }
	 else
	 {
		 ChagerStatusFlag = 2;
	 }	 
	 //status = max14676_Read(MAX14676_STATUS_C);
}


void EnableCHG(void) 
{  
   max14676_GetVer();
   if(!max14676Exist) return;

	 status = max14676_Read(MAX14676_INT_A);
	 status = max14676_Read(MAX14676_INT_B);	
	 status = max14676_Read(MAX14676_STATUS_A);
   status = max14676_Read(MAX14676_STATUS_B);

	 status = max14676_Read(MAX14676_PWR_CFG_ADDR);
   max14676_WriteReg(MAX14676_PWR_CFG_ADDR,  0x80); 
	
	 status = max14676_Read(MAX14676_CHGCNTLB);
   max14676_WriteReg(MAX14676_CHGCNTLB,      0x10);       // Charged Done when charging current is 7.5mA             
	
	 status = max14676_Read(MAX14676_ILIMCNTL_ADDR);
   max14676_WriteReg(MAX14676_ILIMCNTL_ADDR, 0x17);              

	 status = max14676_Read(MAX14676_CHGCNTLA);
   max14676_WriteReg(MAX14676_CHGCNTLA,      0x1C);//0x18              
                                               
	 status = max14676_Read(MAX14676_PCHGCNTL);	
   max14676_WriteReg(MAX14676_PCHGCNTL,      0x6e);      

   max14676_WriteReg(MAX14676_CDETCNTLB,     0x84);              // Charger automatically restarts when Vbat drops below     	 	
	 max14676_WriteReg(MAX14676_CHGVSET,       0xC0);              // 220mV Recharge Threshold in Relation to VBAT


   status = max14676_Read(MAX14676_INT_MASKA);	 
	 max14676_WriteReg(MAX14676_INT_MASKA,     status | CHGSTAT_MASK);     // Enable ChgStat Interrupt

   status = max14676_Read(MAX14676_INT_MASKB);	 
	 max14676_WriteReg(MAX14676_INT_MASKB,     status | USBOK_MASK);   
}


void DisableCHG(void) 
{  
   max14676_GetVer();
   if(!max14676Exist) return;

   status = max14676_Read(MAX14676_INT_MASKA);	 
	 max14676_WriteReg(MAX14676_INT_MASKA,     status & (~CHGSTAT_MASK));     // Disable ChgStat Interrupt

   status = max14676_Read(MAX14676_INT_MASKB);	 
	 max14676_WriteReg(MAX14676_INT_MASKB,     status & (~USBOK_MASK));   
	 
	 status = max14676_Read(MAX14676_INT_A);                                  // Clear Interrupt flag
	 status = max14676_Read(MAX14676_INT_B);		                              // Clear Interrupt flag  
	 
	 max14676_WriteReg(MAX14676_CHGCNTLB,      0x00);                         // Charger disabled
}

void DisbaleInt(void) 
{  
   max14676_GetVer();
   if(!max14676Exist) return;
	
   status = max14676_Read(MAX14676_INT_MASKA);	 
	 max14676_WriteReg(MAX14676_INT_MASKA,     status & (~CHGSTAT_MASK));     // Enable ChgStat Interrupt

   status = max14676_Read(MAX14676_INT_MASKB);	 
	 max14676_WriteReg(MAX14676_INT_MASKB,     status & (~USBOK_MASK));   
	 
	 status = max14676_Read(MAX14676_INT_A);
	 status = max14676_Read(MAX14676_INT_B);		 
}


//return the percentage 
uint8_t max14676_GetBatteryCharge(void) 
{
	uint16_t data=0; 
    uint8_t result;
    result =max14676_ReadReg2(&data,0x04); 
	if(result ==0)
		return (data/256);
	else 
		return 0;
	 
}
uint16_t max14676_GetBatteryChargeVoltage( void) 
{
	  uint16_t data=0; 
  //uint8_t result;
    /*result =*/max14676_ReadReg2(&data,0x02); 
	
	return data;
}


void max14676_GetVer(void) 
{
  uint8_t result;
  uint8_t readValue;  
  
  result =  max14676_ReadReg(MAX14676_CHIP_ID, &readValue);                      

  
  if((result == 0 )&&(readValue == 0x2E))
    max14676Exist = 2;  
  else 
  {
     max14676Exist = 0; 
  }
}
// assume max14676 exist

void max14676_process(void)
{
  uint8_t data;
//uint8_t status;
	uint8_t myCharge;
		
 	//DisableInterrupts; 
 	max14676Voltage = max14676_GetBatteryChargeVoltage(); 
 	myCharge = max14676_GetBatteryCharge();
		
	/*status =  */max14676_ReadReg(MAX14676_STATUS_A, &data);
 	//EnableInterrupts; 
  if(!max14676Charge)
  {
     max14676Charge += myCharge;
	   max14676Charge = max14676Charge>>1;
	}
  max14676Charge = myCharge;   
		
  max14676ChargeStat   = (/*data == 0x5 || */ data == CHARGER_TIMER_DONE) ? 1 : 0;   //  //1 charged; 0 uncharged
  if(max14676Voltage >= 9600)    //3V              
     max14676Voltage_old = max14676Voltage;    
  else
     max14676Voltage = max14676Voltage_old;      
  max14676Voltage = max14676Voltage/625 * 78;
}


void max14676_poweroff(void)
{
	//volatile uint16_t data=0; 	
	for(;;)
	{
		//DisbaleInt();
		HAL_Delay(10);
		max14676_WriteReg(MAX14676_PWR_CFG_ADDR, 0x5);
		HAL_Delay(10);
	}
}
