//*****************************************************************************
//*****************************************************************************
//  FILENAME: cyfi_hal.h
//  Version: 0.99, Updated on 20/07/2013

//
//  DESCRIPTION: <HUB>Star Network Protocol Hardware abstraction layer header
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2013. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#ifndef _CYFI_HAL_H_
#define _CYFI_HAL_H_

#include "cyfi_regs.h"
#include "typedef.h"

#include "iwdg.h"

//#define CYFISNP_100usDelay()       CyDelayUs(100)       // Implement this
#define CYFISNP_Select()             HAL_GPIO_WritePin(Radio_CS_GPIO_Port, Radio_CS_Pin, GPIO_PIN_RESET)
#define CYFISNP_Deselect()           HAL_GPIO_WritePin(Radio_CS_GPIO_Port, Radio_CS_Pin, GPIO_PIN_SET)
#define __RESET_WATCHDOG()           MX_IWDG_RESET()
//#define CYFISNP_ServiceWatchdog()    __RESET_WATCHDOG()//CyWdtClear()

#define DisableInterrupts            __disable_irq()           // Added by Jason Chyen, August 29, 2017
#define EnableInterrupts             __enable_irq()


extern void     CYFISNP_Reset               (void);
extern uint8    CYFISNP_ByteWrite           (uint8 data);
extern void     CYFISNP_Write               (uint8 addr, uint8 data);
extern uint8    CYFISNP_Read                (uint8 adr);
extern void     CYFISNP_SetPtr              (uint8 * ramPtr);
extern void     CYFISNP_SetLength           (uint8 length);
extern void     CYFISNP_FileRead            (uint8 adr, uint8 cnt);
extern void     CYFISNP_FileWrite           (uint8 adr, uint8 cnt);
extern uint8    CYFISNP_ReadStatusDebounced (uint8 adr);
extern uint8    IsCYFISNP_InterruptActive   (void);

#endif




//[] END OF FILE
