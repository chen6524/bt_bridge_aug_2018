//*****************************************************************************
//*****************************************************************************
//  FILENAME: CYFISNP_Protocol.h
//  Version: 0.99, Updated on 20/07/2013

//
//  DESCRIPTION: <HUB>Star Network Protocol Header
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2013. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef CYFISNP_PROTOCOL_H
#define CYFISNP_PROTOCOL_H

//#include <device.h>
#include "CYFISNP_Config.h"
#include "CYFISNP_Eeprom.h"
#include "CYFISNP_Timer.h"

#define BCD_IS_SUPPORTED    (CYFISNP_BCD_PAYLOAD_MAX != 0)

typedef unsigned char BOOL;
// ---------------------------------------------------------------------------
// Node power sources: Wall/lowLatency, alkaline, or coin-cell
// ---------------------------------------------------------------------------
#define CYFISNP_PWR_BAT         0   // Battery with High peak current (alkaline)
#define CYFISNP_PWR_WALL        1   // Wall powered (Node doesn't sleep)
#define CYFISNP_PWR_COIN        2   // Battery with Low peak current (coin-cell)
#define CYFISNP_PWR_TYPE_MASK   0x03

#define GET_DEV_INDEX(dev) ((uint16)(dev) << 3)

extern const uint8 myEEPROM[];

// ---------------------------------------------------------------------------
//
// CYFISNP_eProtState - Overall Protocol State
//
// ---------------------------------------------------------------------------
typedef enum {
    CYFISNP_BIND_MODE           = (uint8_t)0x10,  // Binding (not data Mode)
//  CYFISNP_UNBOUND_MODE        = (uint8_t)0x11,  // (Node only)
//  CYFISNP_CON_MODE            = (uint8_t)0x20,  // (Node only)
//  CYFISNP_CON_MODE_TIMEOUT    = (uint8_t)0x21,  // (Node only)
    CYFISNP_PING_MODE           = (uint8_t)0x30,  // Lost contact with Hub
//  CYFISNP_PING_MODE_TIMEOUT   = (uint8_t)0x31,  // (Node only)
    CYFISNP_data_MODE           = (uint8_t)0x40,  // Last packet was AutoACKed
    CYFISNP_STOP_MODE           = (uint8_t)0x50,
} CYFISNP_PROT_STATE;

extern CYFISNP_PROT_STATE CYFISNP_eProtState;



// ---------------------------------------------------------------------------
//
// CYFISNP_API_PKT - Packet structure between Protocol and API
//
// ---------------------------------------------------------------------------
#define CYFISNP_API_TYPE_MASK           (uint8_t)0xF0    //
#define CYFISNP_API_TYPE_NULL           (uint8_t)0x00   // empty
#define CYFISNP_API_TYPE_BIND_RSP_ACK   (uint8_t)0x01   // Bind Response delivered
#define CYFISNP_API_TYPE_CONF           (uint8_t)0x02   // Deliv confirm, no BCDR
#define CYFISNP_API_TYPE_CONF_BCDR      (uint8_t)0x03   // Deliv confirm, BCDR
#define CYFISNP_API_TYPE_SYNC           (uint8_t)0x04   // Best effort, no BCDR
#define CYFISNP_API_TYPE_SYNC_BCDR      (uint8_t)0x05   // Best effort, BCDR
#define CYFISNP_API_TYPE_PING           (uint8_t)0x06   // Ping from some Node
#define CYFISNP_API_TYPE_BAD_DEV        (uint8_t)0x0E   // Bad devID indication
#define CYFISNP_API_TYPE_UNKNOWN        (uint8_t)0x0F   // Unknown packet

typedef struct {                        //
    uint8_t length;                        // Payload length
    uint8_t rssi;                          // RSSI of packet
    uint8_t type;                          // Packet type
    uint8_t devId;                         // [7:0] = Device ID
    uint8_t payload[14];                   //
} CYFISNP_API_PKT;

typedef struct {                           //
    uint8_t recvBuffer[64];                //
}RECV_BUFFER;

enum CYFISNP_TX_data_PEND {
        CYFISNP_TX_data_EMPTY    = (uint8_t)0,
        CYFISNP_TX_data_ACKED    = (uint8_t)1,
        CYFISNP_TX_data_TIMEOUT  = (uint8_t)2,
        CYFISNP_TX_data_PENDING  = (uint8_t)3
};


                  
// ---------------------------------------------------------------------------
//
// CYFISNP_CurrentChannel - API may view current channel for informative purposes only
//
// ---------------------------------------------------------------------------
extern byte CYFISNP_bCurrentChannel;
extern byte CYFISNP_bCurrentChannel_store;


typedef struct {                // Array offset gives DevId
    #define BCDBUFS_LEN_MASK    (uint8_t)0x3F
    #define BCDBUFS_DELAY_MASK  (uint8_t)0x3F
    #define BCDBUFS_COUNT_MASK  (uint8_t)0x0F
    volatile uint8_t wprCntLen;    // [7:6] = [3:2] 4-bit retry Count
                                   // [5:0] = [5:0] 6-bit "data" len (0-38}
    volatile uint8_t wprCntDly;    // [7:6] = [1:0] 4-bit retry Count (0-15}
                                   // [5:0] = [5:0] 6-bit retry Delay {0-63}
	  volatile uint8_t wprCnt;      

    #define BCDBUFS_PROTOCOL_OFS    (uint8_t)0
    #define BCDBUFS_API_data_OFS    (uint8_t)1
    uint8_t bcd_data[CYFISNP_BCD_PAYLOAD_MAX + 1];
} CYFISNP_BCD_PKT;

// ---------------------------------------------------------------------------
// CYFISNP_IsrRxOverflow - When TRUE, indicates Rx is suspended because Rx packets
//                   are not unloaded fast enough.  Although no AutoAcked
//                   packets are lost, potential Rx packets are ignored.
//
//                   This bit goes FALSE when the overflow condition clears.
// ---------------------------------------------------------------------------
extern BOOL CYFISNP_IsrRxOverflow;


//extern const byte EEPROM[8*(CYFISNP_MAX_NODES + 1)];
//extern const byte EEPROM[8*(127 + 1)];



// ---------------------------------------------------------------------------
//
// Public Functions
//
// ---------------------------------------------------------------------------

#define					CYFISNP_UnbindDevId(id)     CYFISNP_UnbindNodeID(id)
#define					CYFISNP_LookupDevId(mid)    CYFISNP_GetNodeID(mid)
#define					CYFISNP_LookupMid(mid, id)  CYFISNP_GetNodeMid(mid, id)

extern void Cy_DelayUS2(volatile uint32_t microseconds);

byte            CYFISNP_PhyStart        (void);
void            CYFISNP_PhyStop         (void);

byte            CYFISNP_Start           (void);
void            CYFISNP_Stop            (void);
void            CYFISNP_Run             (void);
void            CYFISNP_BindStart       (void);

BOOL            CYFISNP_UnbindNodeID    (byte NodeID);

enum CYFISNP_TX_data_PEND CYFISNP_TxdataPend (byte devId);

BOOL            CYFISNP_TxdataPut       (CYFISNP_API_PKT *ps);

BOOL            CYFISNP_RxdataPend      (void);
CYFISNP_API_PKT * CYFISNP_RxdataGet       (void);
void            CYFISNP_RxdataRelease   (void);

byte            CYFISNP_GetNodeID       (byte *mid);
BOOL            CYFISNP_GetNodeMid      (byte *mid, byte NodeID);
BOOL			      CYFISNP_GetHubMid	      (byte *mid);
//BOOL			      CYFISNP_SetHubMid	      (byte *mid);
BOOL CYFISNP_SetHubMid(CYFISNP_EEP_NET_REC *net);
byte            CYFISNP_GetDieTemp      (void);

void            ClearNetTable(void);

// ---------------------------------------------------------------------------
// The following functions support "cloning" a hub.  Presumably, the host
//  has recorded network and binding information earlier (with a prior hub)
//  and now the application wants to replace the old hub with a new hub
//  without disturbing the nodes.
// ---------------------------------------------------------------------------
//void CYFISNP_netRecWrite(          // Set Network Parameters
//     /*const */uint8 *pRam);       // Ptr to network record to write
//void CYFISNP_devRecWrite(          // Manually bind a device
//     byte devId,                   // Device ID entry to write
//     /*const */uint8 *pRam);       // Ptr to record structure to write

void CYFISNP_netRecWrite(            // Set Network Parameters
     CYFISNP_EEP_NET_REC *pRam);     // Ptr to network record to write
void CYFISNP_devRecWrite(            // Manually bind a device
     byte devId,                     // Device ID entry to write
     CYFISNP_EEP_DEV_REC *pRam);     // Ptr to record structure to write
     
byte numNode_ShelfMode(void);     

byte NetEraseZero(byte eraseIdx);

#endif  // CYFISNP_PROTOCOL_H
// ###########################################################################
