//*****************************************************************************
//*****************************************************************************
//  FILENAME: CYFISNP_Eeprom.h
//  Version:0.99    , Updated on 20/07/2013
//
//  DESCRIPTION: Star Network Protocol HUB EEPROM structures
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2013. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef CYFISNP_EPROM_H_
#define CYFISNP_EPROM_H_

//#include <device.h>

#include "CYFISNP_Config.h"
#include "CYFISNP_Private.h"
#include "CYFISNP_Protocol.h"

//typedef unsigned char BYTE;
// ---------------------------------------------------------------------------
#define CYFISNP_EEP_NET_REC_ADR ((CYFISNP_EEP_NET_REC const *)(myEEPROM))//NET_PARAMETER))
#define CYFISNP_EEP_DEV_REC_ADR ((CYFISNP_EEP_DEV_REC const *)(myEEPROM))//NET_PARAMETER))

// ---------------------------------------------------------------------------
// Power source: Wall, alkaline, or coin-cell
// ---------------------------------------------------------------------------
#define CYFISNP_PWR_BAT     0       // Battery with High peak current (alkaline)
#define CYFISNP_PWR_WALL    1       // Wall powered (Node doesn't sleep)
#define CYFISNP_PWR_COIN    2       // Battery with Low peak current (coin-cell)


// ---------------------------------------------------------------------------
//
// CYFISNP_EEP_DEV_REC - Hub EEPROM Record for each Node Device
//
// ---------------------------------------------------------------------------
#define PKT_FLAGS_MASK  0x0F    // [3:0] of ConReq packet

typedef struct {                    // ---------------------------------------
    uint8_t flg;                       //
    uint8_t devId;                     // [7:0] = device ID (0=unassigned)
    uint8_t devMid[CYFISNP_SIZEOF_MID];    //
    uint8_t rspDelay;                  //
    uint8_t unused_1;                  //
} CYFISNP_EEP_DEV_REC;
// ---------------------------------------------------------------------------
// EXAMPLE USEAGE
//    CYFISNP_EEP_DEV_REC dev;
//    dev.flg       = 0x01;
//    dev.devId     = 0x07;
//    dev.devMid[0] = 0x12;
//    dev.devMid[1] = 0x34;
//    dev.devMid[2] = 0x56;
//    dev.devMid[3] = 0x78;
// ---------------------------------------------------------------------------



// ---------------------------------------------------------------------------
//
// CYFISNP_EEP_NET_REC - Hub or Node EEPROM Network Parameter Record
//
// ---------------------------------------------------------------------------
typedef struct {                // -------------------------------------------
    uint8_t sopIdx;                //
    uint8_t chBase;                //
    uint8_t chHop;                 //
    uint8_t hubSeedMsb;            // Hub CRC Seed[15:8]
    uint8_t hubSeedLsb;            // Hub CRC Seed[ 7:0]
                                // -------------------------------------------
    uint8_t devId;                 // only used by Nodes
    uint8_t nodeSeedMsb;           // used only by Nodes
    uint8_t nodeSeedLsb;           // used only by Nodes
} CYFISNP_EEP_NET_REC;
// ---------------------------------------------------------------------------
// EXAMPLE USEAGE
//    CYFISNP_EEP_NET_REC net;
//    net.sopIdx     = 0xDE;
//    net.chBase     = 0xAD;
//    net.chHop      = 0x55;
//    net.hubSeedMsb = 0xBE;
//    net.hubSeedLsb = 0xEF;
//    net.devId      = 0xA5;
// ---------------------------------------------------------------------------

#endif  // CYFISNP_EPROM_H_
// ###########################################################################
